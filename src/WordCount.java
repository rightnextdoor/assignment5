import java.io.*;
import java.util.Scanner;

public class WordCount {
	
	public static void main(String[] args) {
		int wordCount = 0;
		int lineCount = 0;
		int charsCount = 0;
		
		Scanner myFile = null;
		try{
		Scanner scan = new Scanner(new FileReader("lear.txt"));
		myFile = scan;
		
			} catch (IOException e) {
			System.out.println(e);
		}
		
		while(myFile.hasNextLine()) {
			String line = myFile.nextLine();
			int size = line.length();
			
			for(int i = 0; i < size; i++) {
				if(line.charAt(i) != ' ' && line.charAt(i) != '\'' && line.charAt(i) != ',') {
					charsCount++;
				} else {
					wordCount++;
				}
			}
			lineCount++;	
		}
			
		System.out.println("Line = "+ lineCount);
		System.out.println("Word = "+ wordCount);
		System.out.println("Chars = "+ charsCount);
		
		
	}

}
