import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Histograms {

	public static void main(String[] args) {
		String star1 = "";
		String star2 = "";
		String star3 = "";
		String star4 = "";
		String star5 = "";
		String star6 = "";
		String star7 = "";
		String star8 = "";
		String star9 = "";
		String star10 = "";
		String star11 = "";
		
		Scanner myFile = null;
		try{
		Scanner scan = new Scanner(new FileReader("MidtermScores.txt"));
		myFile = scan;
		
			} catch (IOException e) {
			System.out.println(e);
		}
		
		while(myFile.hasNextInt()) {
			int num = myFile.nextInt();
			
			if(num <= 9) {
			star1 += "*";	
			} else if (num >= 10 && num <= 19) {
				star2 += "*";
			}else if (num >= 20 && num <= 29) {
				star3 += "*";
			}else if (num >= 30 && num <= 39) {
				star4 += "*";
			}else if (num >= 40 && num <= 49) {
				star5 += "*";
			}else if (num >= 50 && num <= 59) {
				star6 += "*";
			}else if (num >= 60 && num <= 69) {
				star7 += "*";
			}else if (num >= 70 && num <= 79) {
				star8 += "*";
			}else if (num >= 80 && num <= 89) {
				star9 += "*";
			}else if (num >= 90 && num <= 99) {
				star10 += "*";
			}else {
				star11 += "*";
			}
			
		}
	
		System.out.println("00-09: " + star1);
		System.out.println("10-19: " + star2);
		System.out.println("20-29: " + star3);
		System.out.println("30-39: " + star4);
		System.out.println("40-49: " + star5);
		System.out.println("50-59: " + star6);
		System.out.println("60-69: " + star7);
		System.out.println("70-79: " + star8);
		System.out.println("80-89: " + star9);
		System.out.println("90-99: " + star10);
		System.out.println("100: " + star11);
	}

}
